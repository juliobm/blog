#!/usr/bin/env bash
# Función: dado un o varios GLOBAL exportados con ^%GOGEN de MUMPS 
#			crea tantos ficheros csv como por índices tenga cada global
# Fecha creación: 30.03.2019
# Autor: Julio Briso-Montiano
# Versión: 1.0
# Detalle:
#   - se pide fichero exportado con %GOGEN
#   - se crean ficheros csv por cada global/índices
#   - se inserta cabecera en el csv del tipo Index1 Index2...Data1 Data2...
#

#
# petición de datos
if [ $# -eq 0 ]
  then
    echo "introduce global exportado con %GOGEN"
    exit
fi
echo -e "\nATENCIÓN: se van a borrar todos los ficheros de tipo global*.csv\n"
read -p "¿estás seguro? (s/n) " vamos
if [[ $vamos != "s" ]]
then
	echo "abortado"
	exit
fi

#
# variables para trabajar
file=$1
sepcsv=";"
global=""
isindex=""
detectedglobal=""
declare -A fic
rm global-*.csv

#
# leemos línea a línea
while IFS= read -r line
do
	#
	# primero tenemos que detectar cuándo es una línea
	# de índices y cuándo es de datos.
	# cuidado que puede haber campos de datos del tipo 
	# ^ALD(x,y,z que parecen de índices
	if [[ "${line:0:18}" == "Transferring files" ]]
	then
		detectedglobal="nextline"
		continue
	fi
	if [[ $detectedglobal == "nextline" ]]
	then
		global="${line//[()$'\r'^]/}"
		echo "global found "$global
		detectedglobal="jump"
		continue
	fi
	if [[ $detectedglobal == "jump" ]]
	then
		detectedglobal=""
		isindex=1
		continue
	fi
	#
	# now we have a global to work on
	# first el de índices
	line="${line//$'\r'/}"
	if [[ $isindex -eq 1 ]]
	then
		isindex=0
		regindex="${line#*(}"
		comas="${regindex//[^,]}"
		indices="${#comas}"
		((indices+=1))
		# hasta aquí para averiguar los índices
		regindex="${regindex//[,)]/$sepcsv}"
		continue
	fi
	# registro de datos que unimos con el de índices y grabamos
	if [[ $isindex -eq 0 ]] && [[ $indices > 0 ]]
	then
		isindex=1
		ficactual=${global}"_"${indices}
		sepas="${line//[^#]}"
		campos="${#sepas}"
		((campos+=1))
		if [[ ${campos} -gt ${fic[$ficactual]} ]]
		then
			fic[$ficactual]=$campos
		fi
		# hasta aquí para averiguar el número máximo de campos para
		# ese global/índice
		regdata="${line//#/$sepcsv}"
		finalreg="${regindex}""${regdata}"
		echo $finalreg >> "global-"$global"_"$indices".csv"
	fi
done <"$file"

#
# insertamos cabecera de nombres
# por cada fichero obtenido
for tipos in "${!fic[@]}"
do
	campos=${fic[$tipos]}
	indices="${tipos#*_}"
	cab=""
	for ((i=1; i<=$indices; i++))
	do
		cab=$cab"Index"$i"\t"
	done
	for ((i=1; i<=$campos; i++))
	do
		cab=$cab"Data"$i"\t"
	done
	# no POSIX
	sed -i "1i"$cab "global-"$tipos".csv"
	echo -e $cab > borrame.txt && cat "global-"$tipos".csv" >> borrame.txt && mv borrame.txt "global-"$tipos".csv"
	sed -i 's/;/\t/g' global-$tipos".csv"
	echo "Generado fichero global-"$tipos".csv"
done
